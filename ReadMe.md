# Drugar

Drugar is an interactive tale about an intergalactic space agency coming across an abandoned satellite. You are Drugar, the resident inspection agent, and are tasked with searching a mysterious satellite and identifying its origins.

## Dependencies

* `NCurses`

## Building

Simply clone the repo, and run `make`. That will create an executable in the root of the project.