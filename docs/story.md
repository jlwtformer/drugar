# Drugar - Story

Drugar is an interactive tale about an intergalactic space agency coming across an abandoned satellite. You are Drugar, the resident inspection agent, and are tasked with searching the satellite and identifying its origins.

## Chapter 0 - The Tutorial

Drugar awakens on the space station in their quarters. They receive an alert on their transponder about a new unidentified satellite. Drugar must now decide whether to organize their tool kit, or catch the mission briefing. This choice will affect the story slightly.

## Chapter 1a - The Briefing (w/ tools)

Drugar arrives to the launch bay a little late, having missed the distribution of the mission briefing. They don their exploration suit, and make the space walk to the seemingly abandoned satellite. When on board, Drugar searches the satellite. They find a locked compartment, and need to activate an on-board pressurization system to open the compartment.

## Chapter 1b - The Briefing (w/o tools)

Drugar arrives to the launch bay, receiving the distribution of the mission briefing. They don their exploration suit, and make the space walk to the seemingly abandoned satellite. When on board, Drugar searches the satellite. They find a locked compartment, and need to activate an on-board pressurization system to open the compartment.

## Chapter 2 - The Discovery

Once the compartment is opened, Drugar discovers a small group of organic creatures attempting to phone home for help. Drugar panics and attempts to flee the satellite, getting lost inside the compartments. Using intel from (a. received from a friend after missing the briefing/b. the briefing files) Drugar manages to make it to the satellite docking bay to warn the others. Drugar is then knocked out cold.

## Chapter 3a - The Escape (w/ tools)

Drugar awakes in a holding bay having been taken prisoner by the creatures, who call themselves human. Drugar makes note of how they are smaller, and are varied in color, unlike Drugar who is green like the rest of his kind. Drugar uses their tools to escape from the cell, and manages to get back to the loading bay of their space station to warn the others of the humans.

## Chapter 3b - The Escape (w/o tools)

Drugar awakes in a holding bay having been taken prisoner by the creatures, who call themselves human. Drugar makes note of how they are smaller, and are varied in color, unlike Drugar who is green like the rest of his kind. Without their tools, Drugar must find a way to escape from the cell, and manage to get back to the loading bay of their space station to warn the others of the humans.

## Chapter 4 - The Finale

As Drugar is being debriefed, the satellite mysteriously disappears into a worm hole and is never seen again. 