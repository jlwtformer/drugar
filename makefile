CC = g++
CFLAGS = -g -Wall
LIBS = -lncurses

all: drugar clean

drugar: drugar.o menu.o game.o story.o level.o handlers.o
	$(CC) $(CFLAGS) -o drugar drugar.o menu.o game.o story.o level.o handlers.o $(LIBS)

drugar.o: src/drugar.cpp src/menu.h src/game.h src/story.h src/level.h src/globals.h src/handlers.h
	$(CC) -c src/drugar.cpp

menu.o: src/menu.h
	$(CC) -c src/menu.cpp

game.o: src/game.h
	$(CC) -c src/game.cpp

story.o: src/story.h
	$(CC) -c src/story.cpp

level.o: src/menu.h src/story.h src/level.h
	$(CC) -c src/level.cpp

handlers.o: src/handlers.h
	$(CC) -c src/handlers.cpp

clean:
	rm *.o