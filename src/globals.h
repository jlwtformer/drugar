#ifndef GLOBALS_H
#define GLOBALS_H

string main_menu_options[3] = {"New Game", "Load Game", "Exit"};
string ch0m1p1[2] = {"Organize Tools", "Answer Urgent Call"};

Menu main_menu = Menu("Main Menu", main_menu_options, 3);

Game main_game = Game();

Story ch0s1[] = {
    Story("Space is a vast expanse, filled to the brim with mostly nothingness. I have been a member of the Intergallactic Expantion Monitoring Agency for for many cycles for many cycles, and am fearing the end of my deployment.", false),
    Story("As I awaken in my quarters on the IEMA Exploration Station, I notice that there is an urgent call awaiting for me on my data gauntlet. Typically I would start my morning by organizing my tools, but maybe I should take this call instead?", true)
};

Menu ch0m1[] = {
    Menu("Should I check the message?", ch0m1p1, 2)
};

Level lvl0 = Level("Level 0", ch0s1, 2, ch0m1, 1);

#endif