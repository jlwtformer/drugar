#ifndef LEVEL_H
#define LEVEL_H

class Level {
    public:
        int map_x;
        int map_y;
        string l_name;
        vector<Story> l_stories;
        vector<Menu> l_menus;
        Level(string name, Story stories[], int s_size, Menu menus[], int m_size);
        void show_level();
};

#endif