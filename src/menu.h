#ifndef MENU_H
#define MENU_H

class Menu {
    public:
        string m_name;
        vector<string> m_options;
        Menu(string name, string options[], int size);
        int show_menu();
};

#endif