#include <ncurses.h>
#include <string>
#include <vector>

using namespace std;

#include "menu.h"

Menu::Menu(string name, string options[], int size) {
    m_name = name;

    for (int i = 0; i < size; i++) {
        m_options.push_back(options[i]);
    }
}

int Menu::show_menu() {
    printw("%s\n\n", m_name.c_str());

    for (int i = 0; i < m_options.size(); i++) {
        int j = i + 1;
        printw("    %i. %s\n", j, m_options[i].c_str());
    }

    printw("\nPlease make a selection...");
    refresh();
    return getch();
}