#include <ncurses.h>
#include <string>
#include <vector>

using namespace std;

#include "menu.h"
#include "game.h"
#include "story.h"
#include "level.h"
#include "globals.h"
#include "handlers.h"

void init_game(int selection);

int main() {
    initscr();
    cbreak();
    noecho();

    clear();
    main_menu_handler(main_menu.show_menu());

    endwin();
    return 0;
}