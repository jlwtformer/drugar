#include <ncurses.h>
#include <string>
#include <vector>

using namespace std;

#include "handlers.h"

int main_menu_handler(int selection) {
    do {
        // Selection values adjusted to match ncurses input values
        if (selection == 49) {
            // TODO: Start a new game
            return 1;
        } else if (selection == 50) {
            // TODO: Load game from a specific chapter
            return 2;
        } else if (selection == 51) {
            return 0;
        } else {
            printw("\nWait, that's illegal\nPlease try again...");
            refresh();
            selection = getch();
        }
    } while (true);
}