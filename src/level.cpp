#include <ncurses.h>
#include <string>
#include <vector>

using namespace std;

#include "menu.h"
#include "story.h"
#include "level.h"

Level::Level(string name, Story stories[], int s_size, Menu menus[], int m_size) {
    l_name = name;

    for (int i = 0; i < s_size; i++) {
        l_stories.push_back(stories[i]);
    }

    for (int i = 0; i < m_size; i++) {
        l_menus.push_back(menus[i]);
    }
}

void Level::show_level() {
    clear();
    int j = 0;

    for (int i = 0; i < l_stories.size(); i++) {
        printw("%s\n\n", l_stories[i].s_clip.c_str());

        if (l_stories[i].hasMenu) {
            l_menus[j].show_menu();
            j++;
        } else {
            printw("Press any key to continue...");
            refresh();
            getch();
        }
    }
}